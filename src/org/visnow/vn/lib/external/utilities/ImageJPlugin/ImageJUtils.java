/* ***** BEGIN LICENSE BLOCK *****
 *  
 * VisNowPlugin-ImageJ
 * Copyright (C) 2015-2019 University of Warsaw, ICM
 * Copyright (C) 2020 onward visnow.org
 * All rights reserved. 
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * ***** END LICENSE BLOCK ***** */

package org.visnow.vn.lib.external.utilities.ImageJPlugin;

import ij.IJ;
import ij.ImagePlus;
import ij.ImageStack;
import java.lang.reflect.Array;
import java.util.ArrayList;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jlargearrays.LargeArray;
import org.visnow.jlargearrays.ShortLargeArray;
import org.visnow.jlargearrays.UnsignedByteLargeArray;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.TimeData;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.dataarrays.DataArrayType;

/**
 * @author Piotr Wendykier (piotrw@icm.edu.pl) Warsaw University, Interdisciplinary Centre for Mathematical and Computational Modelling
 */
public class ImageJUtils
{

    private ImageJUtils()
    {
    }

    /**
     * 
     * @param field
     * @param component
     * @return 
     */
    public static ImagePlus regularFieldToImagePlus(RegularField field, String component)
    {
        DataArray da = field.getComponent(component);
        if (da == null) {
            throw new IllegalArgumentException("Component " + component + " does not exist.");
        }
        if (!da.isNumeric() || da.getType() == DataArrayType.FIELD_DATA_COMPLEX) {
            throw new IllegalArgumentException("Only real numeric components are supported.");
        }
        long[] dims = field.getLDims();
        return dataArrayToImagePlus(da, dims);
    }

    /**
     * 
     * @param type
     * @return 
     */
    public static int dataArrayTypeToIJBitDepth(DataArrayType type)
    {
        switch (type) {
            case FIELD_DATA_LOGIC:
            case FIELD_DATA_BYTE:
                return 8;
            case FIELD_DATA_SHORT:
                return 16;
            case FIELD_DATA_INT:
            case FIELD_DATA_LONG:
            case FIELD_DATA_FLOAT:
            case FIELD_DATA_DOUBLE:
                return 32;
            default:
                throw new IllegalArgumentException("Unsupported data type");
        }
    }

    /**
     *
     * @param da
     * @param dims
     *
     * @return
     */
    public static ImagePlus dataArrayToImagePlus(DataArray da, long[] dims)
    {
        int width = (int) dims[0];
        int height = dims.length < 2 ? 1 : (int) dims[1];
        int depth = dims.length < 3 ? 1 : (int) dims[2];
        int channels = da.getVectorLength();
        TimeData td = da.getTimeData();
        ArrayList<LargeArray> values = td.getValues();
        ImagePlus res = null;
        ImageStack is;
        boolean hyperstack = false;
        if (da.isTimeDependant() || channels > 1) {
            res = IJ.createHyperStack(da.getName(), width, height, channels, depth, td.getNSteps(), dataArrayTypeToIJBitDepth(da.getType()));
            is = res.getImageStack();
        } else {
            is = new ImageStack(width, height, depth);
        }
        for (int f = 0; f < values.size(); f++) {
            LargeArray la = values.get(f);
            switch (dims.length) {
                case 1:
                case 2:
                    switch (da.getType()) {
                        case FIELD_DATA_LOGIC:
                        case FIELD_DATA_BYTE:
                            for (int ch = 0; ch < channels; ch++) {
                                byte[] bdf = new byte[width * height];
                                for (int r = 0; r < height; r++) {
                                    for (int c = 0; c < width; c++) {
                                        bdf[r * width + c] = la.getByte((height - 1 - r) * width * channels + c * channels + ch);
                                    }
                                }
                                is.setPixels(bdf, hyperstack ? res.getStackIndex(ch + 1, 1, f + 1) : 1);
                            }
                            break;
                        case FIELD_DATA_SHORT:
                            for (int ch = 0; ch < channels; ch++) {
                                short[] sdf = new short[width * height];
                                for (int r = 0; r < height; r++) {
                                    for (int c = 0; c < width; c++) {
                                        sdf[r * width + c] = la.getShort((height - 1 - r) * width * channels + c * channels + ch);
                                    }
                                }
                                is.setPixels(sdf, hyperstack ? res.getStackIndex(ch + 1, 1, f + 1) : 1);
                            }
                            break;
                        case FIELD_DATA_INT:
                        case FIELD_DATA_LONG:
                        case FIELD_DATA_FLOAT:
                        case FIELD_DATA_DOUBLE:
                            for (int ch = 0; ch < channels; ch++) {
                                float[] fdf = new float[width * height];
                                for (int r = 0; r < height; r++) {
                                    for (int c = 0; c < width; c++) {
                                        fdf[r * width + c] = la.getFloat((height - 1 - r) * width * channels + c * channels + ch);
                                    }
                                }
                                is.setPixels(fdf, hyperstack ? res.getStackIndex(ch + 1, 1, f + 1) : 1);
                            }
                            break;
                        default:
                            throw new IllegalArgumentException("Unsupported data array");
                    }
                    break;
                case 3:
                    switch (da.getType()) {
                        case FIELD_DATA_LOGIC:
                        case FIELD_DATA_BYTE:
                            for (int ch = 0; ch < channels; ch++) {
                                for (int s = 0; s < depth; s++) {
                                    byte[] bdf = new byte[width * height];
                                    for (int r = 0; r < height; r++) {
                                        for (int c = 0; c < width; c++) {
                                            bdf[r * width + c] = la.getByte(s * width * height * channels + (height - 1 - r) * width * channels + c * channels + ch);
                                        }
                                    }
                                    is.setPixels(bdf, hyperstack ? res.getStackIndex(ch + 1, s + 1, f + 1) : s + 1);
                                }
                            }
                            break;
                        case FIELD_DATA_SHORT:
                            for (int ch = 0; ch < channels; ch++) {
                                for (int s = 0; s < depth; s++) {
                                    short[] sdf = new short[width * height];
                                    for (int r = 0; r < height; r++) {
                                        for (int c = 0; c < width; c++) {
                                            sdf[r * width + c] = la.getShort(s * width * height * channels + (height - 1 - r) * width * channels + c * channels + ch);
                                        }
                                    }
                                    is.setPixels(sdf, hyperstack ? res.getStackIndex(ch + 1, s + 1, f + 1) : s + 1);
                                }
                            }
                            break;
                        case FIELD_DATA_INT:
                        case FIELD_DATA_LONG:
                        case FIELD_DATA_FLOAT:
                        case FIELD_DATA_DOUBLE:
                            for (int ch = 0; ch < channels; ch++) {
                                for (int s = 0; s < depth; s++) {
                                    float[] fdf = new float[width * height];
                                    for (int r = 0; r < height; r++) {
                                        for (int c = 0; c < width; c++) {
                                            fdf[r * width + c] = la.getFloat(s * width * height * channels + (height - 1 - r) * width * channels + c * channels + ch);
                                        }
                                    }
                                    is.setPixels(fdf, hyperstack ? res.getStackIndex(ch + 1, s + 1, f + 1) : s + 1);
                                }
                            }
                            break;
                        default:
                            throw new IllegalArgumentException("Unsupported data array");
                    }
                    break;
                default:
                    throw new IllegalArgumentException("Only 1D, 2D and 3D regular fields are supported.");
            }
        }
        if (!da.isTimeDependant()) {
            res = new ImagePlus(da.getName(), is);
        }
        return res;
    }

    /**
     *
     * @param img
     * @param affine
     * @param channelsMode
     *
     * @return
     */
    public static RegularField imagePlusToRegularField(ImagePlus img, float[][] affine, ImageJPlugin.ChannelsMode channelsMode)
    {
        if (img == null) {
            throw new IllegalArgumentException("img == null");
        }
        long[] dims = null;
        ImageStack is = img.getImageStack();
        switch (img.getNDimensions()) {
            case 1:
                dims = new long[]{is.getWidth()};
                break;
            case 2:
                if (is.getHeight() <= 1) {
                    dims = new long[]{is.getWidth()};
                } else {
                    dims = new long[]{is.getWidth(), is.getHeight()};
                }
                break;
            case 3:
            case 4:
            case 5:
                if (is.getSize() / img.getNFrames() / img.getNChannels() <= 1) {
                    dims = new long[]{is.getWidth(), is.getHeight()};
                } else {
                    dims = new long[]{is.getWidth(), is.getHeight(), is.getSize() / img.getNFrames() / img.getNChannels()};
                }
                break;
            default:
                throw new IllegalArgumentException("Only 1D - 5D images are supported.");
        }
        RegularField field = new RegularField(dims);
        if (affine != null) {
            field.setAffine(affine);
        }
        field.setName("ImageJ");
        DataArray[] das = imagePlusToDataArray(img, channelsMode);
        for (DataArray dataArray : das) {
            field.addComponent(dataArray);
        }
        return field;
    }

    /**
     *
     * @param img
     * @param channelsMode
     *
     * @return
     */
    public static DataArray[] imagePlusToDataArray(ImagePlus img, ImageJPlugin.ChannelsMode channelsMode)
    {
        ImageStack is = img.getImageStack();
        int width = is.getWidth();
        int height = is.getHeight();
        int channels = img.getNChannels();
        int depth = is.getSize() / img.getNFrames() / channels;
        long nelements = width * height * depth * channels;
        ArrayList<Float> timeSeries = new ArrayList<>(img.getNFrames());
        ArrayList<LargeArray> dataSeries = new ArrayList<>(img.getNFrames());
        LargeArray[] la = null;
        DataArray[] da = null;
        ArrayList<LargeArray>[] dataSeriesArray = null;

        if (channelsMode == ImageJPlugin.ChannelsMode.CHANNELS_AS_VECTOR_ELEMENTS) {
            for (int f = 0; f < img.getNFrames(); f++) {
                timeSeries.add((float) f);
                switch (is.getBitDepth()) {
                    case 8: {
                        UnsignedByteLargeArray arr = new UnsignedByteLargeArray(nelements, false);
                        for (int s = 0; s < depth; s++) {
                            for (int ch = 0; ch < channels; ch++) {
                                byte[] sliceData = (byte[]) is.getPixels(img.getStackIndex(ch + 1, s + 1, f + 1));
                                for (int r = 0; r < height; r++) {
                                    for (int c = 0; c < width; c++) {
                                        arr.setByte(s * width * height * channels + r * width * channels + c * channels + ch, sliceData[(height - 1 - r) * width + c]);
                                    }
                                }
                            }
                        }
                        dataSeries.add(arr);
                        if (f == img.getNFrames() - 1) {
                            da = new DataArray[]{DataArray.create(new TimeData(timeSeries, dataSeries, 0), channels, img.getTitle())};
                        }
                        break;
                    }
                    case 16: {
                        ShortLargeArray arr = new ShortLargeArray(nelements, false);
                        for (int s = 0; s < depth; s++) {
                            for (int ch = 0; ch < channels; ch++) {
                                short[] sliceData = (short[]) is.getPixels(img.getStackIndex(ch + 1, s + 1, f + 1));
                                for (int r = 0; r < height; r++) {
                                    for (int c = 0; c < width; c++) {
                                        arr.setShort(s * width * height * channels + r * width * channels + c * channels + ch, sliceData[(height - 1 - r) * width + c]);
                                    }
                                }
                            }
                        }
                        dataSeries.add(arr);
                        if (f == img.getNFrames() - 1) {
                            da = new DataArray[]{DataArray.create(new TimeData(timeSeries, dataSeries, 0), channels, img.getTitle())};
                        }
                        break;
                    }
                    case 32: {
                        FloatLargeArray arr = new FloatLargeArray(nelements, false);
                        for (int s = 0; s < depth; s++) {
                            for (int ch = 0; ch < channels; ch++) {
                                float[] sliceData = (float[]) is.getPixels(img.getStackIndex(ch + 1, s + 1, f + 1));
                                for (int r = 0; r < height; r++) {
                                    for (int c = 0; c < width; c++) {
                                        arr.setFloat(s * width * height * channels + r * width * channels + c * channels + ch, sliceData[(height - 1 - r) * width + c]);
                                    }
                                }
                            }
                        }
                        dataSeries.add(arr);
                        if (f == img.getNFrames() - 1) {
                            da = new DataArray[]{DataArray.create(new TimeData(timeSeries, dataSeries, 0), channels, img.getTitle())};
                        }
                        break;
                    }
                    case 24: {
                        channels = 3;
                        UnsignedByteLargeArray arr = new UnsignedByteLargeArray(channels * nelements, false);
                        int rMask = 0xFF0000, gMask = 0xFF00, bMask = 0xFF;
                        for (int s = 0; s < depth; s++) {
                            int[] sliceData = (int[]) is.getPixels(img.getStackIndex(1, s + 1, f + 1));
                            for (int r = 0; r < height; r++) {
                                for (int c = 0; c < width; c++) {
                                    int code = sliceData[(int) ((height - 1 - r) * width + c)];
                                    arr.setByte(s * width * height * channels + r * width * channels + c * channels, (byte) ((code & rMask) >> 16));
                                    arr.setByte(s * width * height * channels + r * width * channels + c * channels + 1, (byte) ((code & gMask) >> 8));
                                    arr.setByte(s * width * height * channels + r * width * channels + c * channels + 2, (byte) (code & bMask));

                                }
                            }
                        }
                        dataSeries.add(arr);
                        if (f == img.getNFrames() - 1) {
                            da = new DataArray[]{DataArray.create(new TimeData(timeSeries, dataSeries, 0), channels, img.getTitle())};
                        }
                        break;
                    }
                    default:
                        throw new IllegalArgumentException("Unsupported type of ImageStack.");
                }
            }
        } else {
            nelements /= channels;
            if (is.getBitDepth() == 24) {
                channels = 3;
            }
            for (int f = 0; f < img.getNFrames(); f++) {
                timeSeries.add((float) f);
                if (f == 0) {
                    dataSeriesArray = (ArrayList<LargeArray>[]) Array.newInstance(ArrayList.class, channels);
                    for (int ch = 0; ch < channels; ch++) {
                        dataSeriesArray[ch] = new ArrayList<>(img.getNFrames());
                    }
                }
                la = new LargeArray[channels];
                switch (is.getBitDepth()) {
                    case 8: {
                        for (int ch = 0; ch < channels; ch++) {
                            la[ch] = new UnsignedByteLargeArray(nelements, false);
                        }
                        for (int ch = 0; ch < channels; ch++) {
                            for (int s = 0; s < depth; s++) {
                                byte[] sliceData = (byte[]) is.getPixels(img.getStackIndex(ch + 1, s + 1, f + 1));
                                for (int r = 0; r < height; r++) {
                                    for (int c = 0; c < width; c++) {
                                        la[ch].setByte(s * width * height + r * width + c, sliceData[(height - 1 - r) * width + c]);
                                    }
                                }
                            }
                            dataSeriesArray[ch].add(la[ch]);
                        }
                        if (f == img.getNFrames() - 1) {
                            da = new DataArray[channels];
                            for (int ch = 0; ch < channels; ch++) {
                                da[ch] = DataArray.create(new TimeData(timeSeries, dataSeriesArray[ch], 0), 1, img.getTitle() + (channels > 1 ? ("_channel" + (ch + 1)) : ""));
                            }
                        }
                        break;
                    }
                    case 16: {
                        for (int ch = 0; ch < channels; ch++) {
                            la[ch] = new ShortLargeArray(nelements, false);
                        }
                        for (int ch = 0; ch < channels; ch++) {
                            for (int s = 0; s < depth; s++) {
                                short[] sliceData = (short[]) is.getPixels(img.getStackIndex(ch + 1, s + 1, f + 1));
                                for (int r = 0; r < height; r++) {
                                    for (int c = 0; c < width; c++) {
                                        la[ch].setShort(s * width * height + r * width + c, sliceData[(height - 1 - r) * width + c]);
                                    }
                                }
                            }
                            dataSeriesArray[ch].add(la[ch]);
                        }
                        if (f == img.getNFrames() - 1) {
                            da = new DataArray[channels];
                            for (int ch = 0; ch < channels; ch++) {
                                da[ch] = DataArray.create(new TimeData(timeSeries, dataSeriesArray[ch], 0), 1, img.getTitle() + (channels > 1 ? ("_channel" + (ch + 1)) : ""));
                            }
                        }
                        break;
                    }
                    case 32: {
                        for (int ch = 0; ch < channels; ch++) {
                            la[ch] = new FloatLargeArray(nelements, false);
                        }
                        for (int ch = 0; ch < channels; ch++) {
                            for (int s = 0; s < depth; s++) {
                                float[] sliceData = (float[]) is.getPixels(img.getStackIndex(ch + 1, s + 1, f + 1));
                                for (int r = 0; r < height; r++) {
                                    for (int c = 0; c < width; c++) {
                                        la[ch].setFloat(s * width * height + r * width + c, sliceData[(height - 1 - r) * width + c]);
                                    }
                                }
                            }
                            dataSeriesArray[ch].add(la[ch]);
                        }
                        if (f == img.getNFrames() - 1) {
                            da = new DataArray[channels];
                            for (int ch = 0; ch < channels; ch++) {
                                da[ch] = DataArray.create(new TimeData(timeSeries, dataSeriesArray[ch], 0), 1, img.getTitle() + (channels > 1 ? ("_channel" + (ch + 1)) : ""));
                            }
                        }
                        break;
                    }
                    case 24: {
                        for (int ch = 0; ch < channels; ch++) {
                            la[ch] = new UnsignedByteLargeArray(nelements, false);
                        }
                        int rMask = 0xFF0000, gMask = 0xFF00, bMask = 0xFF;
                        for (int i = 1; i <= depth; i++) {
                            int[] sliceData = (int[]) is.getPixels(img.getStackIndex(1, i, f + 1));
                            for (int r = 0; r < height; r++) {
                                for (int c = 0; c < width; c++) {
                                    int code = sliceData[(int) ((height - 1 - r) * width + c)];
                                    la[0].setByte((i - 1) * width * height + r * width + c, (byte) ((code & rMask) >> 16));
                                    la[1].setByte((i - 1) * width * height + r * width + c, (byte) ((code & gMask) >> 8));
                                    la[2].setByte((i - 1) * width * height + r * width + c, (byte) (code & bMask));
                                }
                            }
                        }
                        dataSeriesArray[0].add(la[0]);
                        dataSeriesArray[1].add(la[1]);
                        dataSeriesArray[2].add(la[2]);
                        if (f == img.getNFrames() - 1) {
                            da = new DataArray[3];
                            da[0] = DataArray.create(new TimeData(timeSeries, dataSeriesArray[0], 0), 1, img.getTitle() + "_red");
                            da[1] = DataArray.create(new TimeData(timeSeries, dataSeriesArray[1], 0), 1, img.getTitle() + "_green");
                            da[2] = DataArray.create(new TimeData(timeSeries, dataSeriesArray[2], 0), 1, img.getTitle() + "_blue");
                        }
                        break;
                    }
                    default:
                        throw new IllegalArgumentException("Unsupported type of ImageStack.");
                }
            }
        }
        return da;
    }
}
