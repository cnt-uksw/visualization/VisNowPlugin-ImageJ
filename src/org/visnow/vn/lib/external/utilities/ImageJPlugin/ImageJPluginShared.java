/* ***** BEGIN LICENSE BLOCK *****
 *  
 * VisNowPlugin-ImageJ
 * Copyright (C) 2015-2019 University of Warsaw, ICM
 * Copyright (C) 2020 onward visnow.org
 * All rights reserved. 
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * ***** END LICENSE BLOCK ***** */

package org.visnow.vn.lib.external.utilities.ImageJPlugin;

import org.visnow.jscic.RegularFieldSchema;
import org.visnow.vn.engine.core.Parameter;
import org.visnow.vn.engine.core.ParameterName;

public class ImageJPluginShared
{

    public static final ParameterName<String> PLUGINS_DIR = new ParameterName<>("IJ plugins directory");
    public static final ParameterName<String> COMPONENT1 = new ParameterName<>("Component1");
    public static final ParameterName<String> COMPONENT2 = new ParameterName<>("Component2");
    public static final ParameterName<ImageJPlugin.ChannelsMode> CHANNELS_MODE = new ParameterName("Channels mode");
    public static final ParameterName<RegularFieldSchema> META_FIELD1_SCHEMA = new ParameterName("META field1 schema");
    public static final ParameterName<RegularFieldSchema> META_FIELD2_SCHEMA = new ParameterName("META field2 schema");
    public static final ParameterName<Boolean> GENERATE_OUTPUT_FIELD = new ParameterName<>("Generate output field");
    public static final ParameterName<Boolean> SHOW_IMAGEJ_WINDOW = new ParameterName<>("Show ImageJ window");

    public static Parameter[] getDefaultParameters()
    {
        return new Parameter[]{
            new Parameter<>(PLUGINS_DIR, ""),
            new Parameter<>(COMPONENT1, ""),
            new Parameter<>(COMPONENT2, ""),
            new Parameter<>(CHANNELS_MODE, ImageJPlugin.ChannelsMode.CHANNELS_AS_SEPARATE_COMPONENTS),
            new Parameter<>(GENERATE_OUTPUT_FIELD, false),
            new Parameter<>(SHOW_IMAGEJ_WINDOW, false),
            new Parameter<>(META_FIELD1_SCHEMA, null),
            new Parameter<>(META_FIELD2_SCHEMA, null)
        };
    }

}
