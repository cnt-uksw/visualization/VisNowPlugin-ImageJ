/* ***** BEGIN LICENSE BLOCK *****
 *  
 * VisNowPlugin-ImageJ
 * Copyright (C) 2015-2019 University of Warsaw, ICM
 * Copyright (C) 2020 onward visnow.org
 * All rights reserved. 
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * ***** END LICENSE BLOCK ***** */

package org.visnow.vn.lib.external.utilities.ImageJPlugin;

import ij.IJ;
import ij.ImageJ;
import ij.ImagePlus;
import ij.Menus;
import ij.gui.ImageWindow;
import ij.WindowManager;
import ij.gui.StackWindow;
import java.util.Arrays;
import org.apache.log4j.Logger;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.RegularFieldSchema;
import org.visnow.jscic.dataarrays.DataArraySchema;
import org.visnow.jscic.dataarrays.DataArrayType;
import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.lib.templates.visualization.modules.OutFieldVisualizationModule;
import org.visnow.vn.lib.types.VNRegularField;
import org.visnow.vn.lib.utils.SwingInstancer;
import org.visnow.vn.engine.core.Parameter;
import org.visnow.vn.engine.core.ParameterChangeListener;
import org.visnow.vn.engine.core.Parameters;
import static org.visnow.vn.lib.external.utilities.ImageJPlugin.ImageJPluginShared.*;
import static org.visnow.vn.lib.external.utilities.ImageJPlugin.ImageJUtils.*;
import org.visnow.vn.system.main.VisNow;

/**
 * @author Piotr Wendykier (piotrw@icm.edu.pl) Warsaw University, Interdisciplinary Centre for Mathematical and Computational Modelling
 */
public class ImageJPlugin extends OutFieldVisualizationModule
{

    private static final Logger LOGGER = Logger.getLogger(ImageJPlugin.class);
    private GUI computeUI = null;
    private RegularField inField1 = null;
    private RegularField inField2 = null;
    private ImagePlus imp1 = null;
    private ImagePlus imp2 = null;
    private ImageWindow win1 = null;
    private ImageWindow win2 = null;
    private ImageJ ij = null;
    private String previousComponent1 = "";
    private String previousComponent2 = "";
    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;

    /**
     * Creates a new instance of ReadImage
     */
    public ImageJPlugin()
    {
        parameters.addParameterChangelistener(new ParameterChangeListener()
        {
            @Override
            public void parameterChanged(String name)
            {
                if (name.equals(GENERATE_OUTPUT_FIELD.getName()) && parameters.get(GENERATE_OUTPUT_FIELD) == true) {
                    updateOutputField(parameters.get(CHANNELS_MODE));
                }
                else if (name.equals(SHOW_IMAGEJ_WINDOW.getName()) && parameters.get(SHOW_IMAGEJ_WINDOW) == true) {
                    if(ij != null) {
                        ij.setVisible(true);
                    }
                } else if (name.equals(PLUGINS_DIR.getName())) {
                    System.setProperty("plugins.dir", parameters.get(PLUGINS_DIR));
                    Menus.updateImageJMenus();
                } else if (name.equals(COMPONENT1.getName()) || name.equals(COMPONENT2.getName())) {
                    startAction();
                }
            }
        });
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                computeUI = new GUI();
                ui.addComputeGUI(computeUI);
                setPanel(ui);
                computeUI.setParameters(parameters);
            }
        });
    }

    static enum ChannelsMode
    {

        CHANNELS_AS_SEPARATE_COMPONENTS("As components"),
        CHANNELS_AS_VECTOR_ELEMENTS("As vector elements");

        final String text;

        private ChannelsMode(String text)
        {
            this.text = text;
        }

        @Override
        public String toString()
        {
            return text;
        }
    }

    @Override
    protected Parameter[] createDefaultParameters()
    {
        return ImageJPluginShared.getDefaultParameters();
    }

    private void validateParamsAndSetSmart(boolean resetParameters)
    {
        if (resetParameters) {
            parameters.setParameterActive(false);
            parameters.set(CHANNELS_MODE, ChannelsMode.CHANNELS_AS_SEPARATE_COMPONENTS);
            parameters.set(PLUGINS_DIR, VisNow.get().getMainConfig().getUsableDataPath(ImageJPlugin.class));
            parameters.set(GENERATE_OUTPUT_FIELD, false);
            if (inField1 != null) {
                String[] numericComponents1 = inField1.getNumericComponentNames();
                String component1 = numericComponents1 != null && numericComponents1.length > 0 ? numericComponents1[0] : "";
                RegularFieldSchema schema1 = inField1.getSchema().cloneDeep();
                for (DataArraySchema componentSchema : inField1.getSchema().getComponentSchemas()) {
                    if (componentSchema.getType() == DataArrayType.FIELD_DATA_COMPLEX) {
                        schema1.removeDataArraySchema(componentSchema);
                    }
                }
                parameters.set(COMPONENT1, component1);
                parameters.set(META_FIELD1_SCHEMA, schema1);
            } else {
                parameters.set(COMPONENT1, "");
                parameters.set(META_FIELD1_SCHEMA, null);
            }
            if (inField2 != null) {
                String[] numericComponents2 = inField2.getNumericComponentNames();
                String component2 = numericComponents2 != null && numericComponents2.length > 0 ? numericComponents2[0] : "";
                RegularFieldSchema schema2 = inField2.getSchema().cloneDeep();
                for (DataArraySchema componentSchema : inField2.getSchema().getComponentSchemas()) {
                    if (componentSchema.getType() == DataArrayType.FIELD_DATA_COMPLEX) {
                        schema2.removeDataArraySchema(componentSchema);
                    }
                }
                parameters.set(COMPONENT2, component2);
                parameters.set(META_FIELD2_SCHEMA, schema2);
            } else {
                parameters.set(COMPONENT2, "");
                parameters.set(META_FIELD2_SCHEMA, null);
            }

            parameters.setParameterActive(true);
        }
    }

    @Override
    protected void notifySwingGUIs(org.visnow.vn.engine.core.ParameterProxy clonedParameterProxy, boolean resetFully, boolean setRunButtonPending)
    {
        computeUI.updateGUI(clonedParameterProxy, resetFully, setRunButtonPending);
    }

    @Override
    public void onDelete()
    {
        super.onDelete();
        WindowManager.closeAllWindows();
        /*Windows created by plugins cannot be disposed. See https://forum.image.sc/t/closing-java-windows-opened-by-plugins-from-imagej-macro/8067*/
        if (ij != null) {
            ij.quit();
        }
    }

    @Override
    public void onActive()
    {
        LOGGER.debug(isFromVNA() + " inField: " + getInputFirstValue("inField"));
        boolean isDifferentField1 = false;
        boolean isNewField1 = false;
        boolean isDifferentField2 = false;
        boolean isNewField2 = false;
        if (getInputFirstValue("inField1") != null) {
            RegularField newInField1 = ((VNRegularField) getInputFirstValue("inField1")).getField();
            isDifferentField1 = !isFromVNA() && (inField1 == null || !Arrays.equals(newInField1.getComponentNames(), inField1.getComponentNames()));
            isNewField1 = !isFromVNA() && (inField1 == null || newInField1 != inField1);
            inField1 = newInField1;
        } else {
            inField1 = null;
        }
        if (getInputFirstValue("inField2") != null) {
            RegularField newInField2 = ((VNRegularField) getInputFirstValue("inField2")).getField();
            isDifferentField2 = !isFromVNA() && (inField2 == null || !Arrays.equals(newInField2.getComponentNames(), inField2.getComponentNames()));
            isNewField2 = !isFromVNA() && (inField2 == null || newInField2 != inField2);
            inField2 = newInField2;
        } else {
            inField2 = null;
        }

        Parameters p;
        synchronized (parameters) {
            validateParamsAndSetSmart(isDifferentField1 || inField1 == null || isDifferentField2 || inField2 == null);
            p = parameters.getReadOnlyClone();
        }

        notifyGUIs(p, isFromVNA() || isDifferentField1 || isDifferentField2, isFromVNA() || isNewField1 || isNewField2);

        if (inField1 != null) {
            if (isNewField1 || isDifferentField1 || !previousComponent1.equals(p.get(COMPONENT1))) {
                imp1 = regularFieldToImagePlus(inField1, p.get(COMPONENT1));
                previousComponent1 = p.get(COMPONENT1);
            }
        } else {
            imp1 = null;
            if (win1 != null) {
                if (win1.isVisible()) {
                    win1.dispose();
                }
                win1 = null;
            }
        }

        if (inField2 != null) {
            if (isNewField2 || isDifferentField2 || !previousComponent2.equals(p.get(COMPONENT2))) {
                imp2 = regularFieldToImagePlus(inField2, p.get(COMPONENT2));
                previousComponent2 = p.get(COMPONENT2);
            }
        } else {
            imp2 = null;
            if (win2 != null) {
                if (win2.isVisible()) {
                    win2.dispose();
                }
                win2 = null;
            }
        }

        if (ij == null) {
            System.setProperty("plugins.dir", p.get(PLUGINS_DIR));
            Menus.updateImageJMenus();
            ij = new ImageJ();
        }

        if (imp2 != null) {
            if (win2 == null) {
                win2 = imp2.getStackSize() > 1 ? new StackWindow(imp2) : new ImageWindow(imp2);
                WindowManager.setCurrentWindow(win2);
            } else {
                win2.setImage(imp2);
                win2.repaint();
            }
            WindowManager.setTempCurrentImage(imp2);
            WindowManager.repaintImageWindows();
        }

        if (imp1 != null) {
            if (win1 == null) {
                win1 = imp1.getStackSize() > 1 ? new StackWindow(imp1) : new ImageWindow(imp1);
                WindowManager.setCurrentWindow(win1);
            } else {
                win1.setImage(imp1);
                win1.repaint();
            }
            WindowManager.setTempCurrentImage(imp1);
            WindowManager.repaintImageWindows();
        }
    }

    private void updateOutputField(ChannelsMode channelsMode)
    {
        ImagePlus imp = WindowManager.getCurrentImage();
        if (imp == null) {
            IJ.noImage();
            outField = null;
            setOutputValue("outField", null);
            show();
        } else {
            outField = outRegularField = imagePlusToRegularField(imp, (inField1 != null) ? inField1.getAffine() : null, channelsMode);
            setOutputValue("outField", new VNRegularField(outRegularField));
            prepareOutputGeometry();
            show();
        }
    }

}
